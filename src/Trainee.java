public class Trainee {

    private int str = 10;
    private int stamina = 100;

    public Trainee(int str, int stamina){
        this.str = str;
        this.stamina = stamina;
    }

    public boolean pushUp(){
        if(stamina >= 10){
            str++;
            stamina = stamina - 10;
            System.out.println("VARU");
            return true;
        }else{
            System.out.println("Nevaru");
            return false;
        }
    }

    public void eat(Pancakes pancakes){
        stamina = stamina + pancakes.getStamina();
        System.out.println("MMM pancakes");
    }

    public void eat(Sup sup){
        stamina = stamina + sup.getStamina();
        System.out.println("sup");
    }

    public void setStr(int str){
        this.str = str;
    }

    public int getStr() {
        return str;
    }

    public int getStamina() {
        return stamina;
    }
}
